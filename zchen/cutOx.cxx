{
//========= Macro generated from object: cutOx/Graph
//========= by ROOT version6.26/06
   
   TCutG *cutg = new TCutG("cutOx",13);
   cutg->SetVarX(" E, with Prisma-Gamma time gate");
   cutg->SetVarY("total DE_AB ");
   cutg->SetTitle("Graph");
   cutg->SetFillStyle(1000);
   cutg->SetPoint(0,6701.29,1077.63);
   cutg->SetPoint(1,4749.28,1446.05);
   cutg->SetPoint(2,2564.47,2127.63);
   cutg->SetPoint(3,2474.93,2256.58);
   cutg->SetPoint(4,2671.92,2625);
   cutg->SetPoint(5,3298.71,2385.53);
   cutg->SetPoint(6,4194.13,2017.11);
   cutg->SetPoint(7,5232.81,1814.47);
   cutg->SetPoint(8,6969.91,1519.74);
   cutg->SetPoint(9,7292.26,1409.21);
   cutg->SetPoint(10,7149,1169.74);
   cutg->SetPoint(11,6898.28,1114.47);
   cutg->SetPoint(12,6701.29,1077.63);
   cutg->Draw("");
}
