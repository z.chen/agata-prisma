//
//last updated@27Apr,2023
//z.chen
//a simple script for nearline analysis
//root -l ana.cxx

TFile *gIptFile;
TCanvas *gCan;
TCutG *cutg;

inline void help(){
	std::cout<<"================================================"<<std::endl;
	std::cout<<"====           	Useful info.                ===="<<std::endl;
	std::cout<<"=== 1.You always need to load data first.   ===="<<std::endl;
	std::cout<<"=== 2.To load data:  root [0]load()         ===="<<std::endl;
	std::cout<<"=== 3.To check PRISMA counting rate:        ===="<<std::endl;
	std::cout<<"=== root [1] prismaCountingRateCalc()       ===="<<std::endl;
	std::cout<<"=== 4.To check xfpFocal plane:              ===="<<std::endl;
	std::cout<<"=== root [2] xfpFocalCalc()                 ===="<<std::endl;
	std::cout<<"=== 5.To check gamma statistic from Coulex: ===="<<std::endl;
	std::cout<<"=== root [3] beamCoulexCalc()               ===="<<std::endl;
	std::cout<<"=== 6.To check O isotopes statistic from IC:===="<<std::endl;
	std::cout<<"=== root [4] oxFromIC()                     ===="<<std::endl;
	std::cout<<"=== 7.To calculate Ne/O rates:              ===="<<std::endl;
	std::cout<<"=== root [5] NeOverOx()                     ===="<<std::endl;
	std::cout<<"=== 8.To calculate AGATA-PRISMA coin:       ===="<<std::endl;
	std::cout<<"=== root [6] agataPRISMA_coinCheck()        ===="<<std::endl;
	std::cout<<"=== 9.To check gamma spec with Qval gate:   ===="<<std::endl;
	std::cout<<"=== root [7] gammaSpecWithQgate()           ===="<<std::endl;
	std::cout<<"================================================"<<std::endl;
}

void load(int frun = 30, int fsub_number = 3){

	gIptFile = new TFile(Form("./Out/sum-%d_%d.root",frun,fsub_number));
	if(!gIptFile->IsOpen()){
		std::cout<<"*Error401!!! Can not open data file "<<Form("./Out/sum-%d_%d.root",frun,fsub_number)<<". Please check file name and path."<<std::endl;
		return;
	}
	gCan = new TCanvas("ana","ana",10,10,1800,1200);
	gCan->Divide(3,3);
	std::cout<<"================================================"<<std::endl;
	std::cout<<"* new Canvas has been created."<<std::endl;
	std::cout<<"* current run is "<<frun<<std::endl;
	std::cout<<"================================================"<<std::endl;

	cutg = new TCutG("cutOx",13);
	cutg->SetVarX(" E, with Prisma-Gamma time gate");
	cutg->SetVarY("total DE_AB ");
	cutg->SetTitle("Graph");
	cutg->SetFillStyle(1000);
	cutg->SetPoint(0,6701.29,1077.63);
	cutg->SetPoint(1,4749.28,1446.05);
	cutg->SetPoint(2,2564.47,2127.63);
	cutg->SetPoint(3,2474.93,2256.58);
	cutg->SetPoint(4,2671.92,2625);
	cutg->SetPoint(5,3298.71,2385.53);
	cutg->SetPoint(6,4194.13,2017.11);
	cutg->SetPoint(7,5232.81,1814.47);
	cutg->SetPoint(8,6969.91,1519.74);
	cutg->SetPoint(9,7292.26,1409.21);
	cutg->SetPoint(10,7149,1169.74);
	cutg->SetPoint(11,6898.28,1114.47);
}

void prismaCountingRateCalc(){

	gCan->cd(1);
	TH1F *fhPRISMA = (TH1F*) gIptFile->Get("Prisma/Ana/h_CR");
	fhPRISMA->SetName("prisma counting rate");
	fhPRISMA->Draw("hist");
	fhPRISMA->GetXaxis()->SetRangeUser(10,20000);
	int fcnt = 0;
	//int fjj = 0;//only for test
	ULong64_t frates=0;
	//std::cout<< "nentry = "<<fhPRISMA->GetEntries()<<std::endl;
	//std::cout<< "i content = "<<fhPRISMA->GetBinContent(10)<<std::endl;
	//std::cout<<"bin width = "<<fhPRISMA->GetBinWidth(100)<<std::endl;
	int fthe = 40;
	for(int i = 1; i<fhPRISMA->GetNbinsX();i++){
		if(fhPRISMA->GetBinContent(i)>fthe){
			frates+=fhPRISMA->GetBinContent(i);
			fcnt++;
		}
		//fjj++;//only for test
	}
	Int_t binWidth = fhPRISMA->GetBinWidth(100);
	std::cout<<"================================================"<<std::endl;
	std::cout<<"*total counts = "<<frates*binWidth<<std::endl;
	std::cout<<"*threshold for real time calculation is "<<fthe<<std::endl;
	std::cout<<"*Real run duration = "<<fcnt*binWidth<<" s"<<std::endl;
	std::cout<<"*Average PRISMA rate for this run is "<<(float)frates/(fcnt)<<std::endl;
	std::cout<<"================================================"<<std::endl;
	//std::cout<<"*Average PRISMA rate for this run is "<<(float)frates/(fcnt*binWidth)<<std::endl;

	//test
	//std::cout<<"N of loop = "<<fjj<<std::endl;
	//std::cout<<"N of loop2 = "<<fcnt<<std::endl;
	//std::cout<<"bin content = "<<fhPRISMA->GetBinContent(1000000)<<std::endl;
}

void xfpFocalCalc(){

	gCan->cd(2);
	TH1F *fhxfpFocal = (TH1F*) gIptFile->Get("Prisma/Ana/h_XFP");
	fhxfpFocal->SetName("xfpFocal counts");
	fhxfpFocal->Draw();
	int frang[2] = {100,1000};
	int fbins[2];
	fbins[0] = fhxfpFocal->GetBin(frang[0]);
	fbins[1] = fhxfpFocal->GetBin(frang[1]);
	fhxfpFocal->GetXaxis()->SetRangeUser(frang[0],frang[1]);
	gPad->SetLogy();
	ULong64_t fnenties = fhxfpFocal->GetEntries();
	ULong64_t fncounts = fhxfpFocal->Integral(fbins[0],fbins[1]);
	std::cout<<"================================================"<<std::endl;
	std::cout<<"*total entries = "<<fnenties<<std::endl;
	std::cout<<"*total counts = "<<fncounts<<std::endl;
	std::cout<<"================================================"<<std::endl;

}
//2+ in 22Ne
Double_t beamCoulexCalc(){

	gCan->cd(3);
	TH1F *fhDCspec = (TH1F*) gIptFile->Get("AgataPrisma/Ion_Cuts_Z10_A22/h_DC_ion_22_10");
	fhDCspec->SetName("22Ne 2+ state");
	fhDCspec->Draw();
	int frang[2] = {1000,1500};
	fhDCspec->GetXaxis()->SetRangeUser(frang[0],frang[1]);
	TF1 *ff = new TF1("ff","pol0+gaus(1)",frang[0],frang[1]);
	ff->SetParameter(0,10);
	ff->SetParameter(1,100);
	ff->SetParameter(2,1271);
	ff->SetParameter(3,5);
	fhDCspec->Fit("ff","R");
	Double_t fpars[6];
	ff->GetParameters(fpars);
	Double_t fbkg = fpars[0];
	Double_t fmean = fpars[2];
	Double_t fsigma = fpars[3];
	Int_t binWidth = fhDCspec->GetBinWidth(100);
	int fbins[2];
	fbins[0] = fhDCspec->GetBin(fmean-3*fsigma);
	fbins[1] = fhDCspec->GetBin(fmean+3*fsigma);
	Double_t ftotalArea = ff->Integral(fmean-3*fsigma,fmean+3*fsigma)/binWidth;
	//Double_t ftotalArea = fhDCspec->Integral(fbins[0],fbins[1]);
	Double_t fbkgArea = fbkg*6*fsigma/binWidth;
	Double_t fnetArea = ftotalArea - fbkgArea;
	std::cout<<"================================================"<<std::endl;
	std::cout<<"*background area = "<<fbkgArea<<std::endl;
	std::cout<<"*total area = "<<ftotalArea<<std::endl;
	std::cout<<"*net area = "<<fnetArea<<std::endl;
	std::cout<<"================================================"<<std::endl;
	return fnetArea;
}

Double_t oxFromIC(){

	gCan->cd(4);
	TH2F *fhoxFromIC = (TH2F*) gIptFile->Get("AgataPrisma/m_ICDEAB_ICE");
	fhoxFromIC->SetName("O counts from IC");
	fhoxFromIC->Draw("colz");
	cutg->Draw("same");
	//m_ICDEAB_ICE->Draw("colz");
	Double_t fcnt = cutg->IntegralHist(fhoxFromIC);
	std::cout<<"================================================"<<std::endl;
	std::cout<<" counts =  "<<fcnt<<std::endl;
	std::cout<<"================================================"<<std::endl;
	return fcnt;
}

void NeOverOx(){
	Double_t Ne22 = beamCoulexCalc();
	Double_t Ox = oxFromIC();
	Double_t cal1 = Ne22/Ox;
	std::cout<<"================================================"<<std::endl;
	std::cout<<"cal N_Ne/N_O = "<<cal1<<std::endl;
	std::cout<<"================================================"<<std::endl;
	//Double_t cal1 = beamCoulexCalc()/
}

void browser(){
	TBrowser *fbr = new TBrowser();
}

void time_estimation_fromTSHit(Int_t run_number = 30, Double_t fthe = 40){

	Double_t total_time  = 0.;
	TChain *fch = new TChain("TreeMaster");

	fch->Add(Form("Replay/run_%04d/Out/Analysis/Tree_*.root",run_number));
	std::cout<<"*N of total events = "<<fch->GetEntries()<<std::endl;

	fch->Draw("TSHit/(1e8)>>h(5000000,0,5000000)");

	TH1F *fhts = (TH1F *)gROOT->FindObject("h")->Clone();
	fhts->Draw();

	for(int i=1; i < fhts->GetNbinsX() ; i++ ) { 
		if(fthe < fhts->GetBinContent(i) ) total_time++ ;  
	}

	fhts->GetXaxis()->SetRangeUser(2, fhts->GetBinCenter(fhts->GetNbinsX()));
	std::cout<<"================================================"<<std::endl;
	std::cout<<"*threshold for real time calculation is "<<fthe<<std::endl;
	std::cout<<"*Real run duration is "<<total_time<<" s"<< std::endl;
	std::cout<<"================================================"<<std::endl;
}

void agataPRISMA_coinCheck(){
	gCan->cd(5);
	TH1F *fhcoin = (TH1F*) gIptFile->Get("AgataPrisma/h_TSprismamTSgamma");
	fhcoin->SetName("AGATA-PRISMA coin TS");
	fhcoin->Draw();
	int frang[2] = {-40,40};
	//fhDCspec->GetXaxis()->SetRangeUser(frang[0],frang[1]);
	TF1 *ff = new TF1("ff","pol0+gaus(1)",frang[0],frang[1]);
	ff->SetParameter(0,1000);
	ff->SetParameter(1,3000);
	ff->SetParameter(2,11);
	ff->SetParameter(3,1);
	ff->SetParLimits(3,1,3);
	fhcoin->Fit("ff","R");
	Double_t fpars[6];
	ff->GetParameters(fpars);
	Double_t famp = fpars[1];
	Double_t frandom = fpars[0];
	Double_t fratio = famp/frandom;//
	std::cout<<"================================================"<<std::endl;
	std::cout<<"*amplitude of coin peak = "<<famp<<std::endl;
	std::cout<<"*random level = "<<frandom<<std::endl;
	std::cout<<"*Ratio = "<<fratio<<std::endl;
	if(fratio<1.5)std::cout<<"*Warning401!!! AGATA-PRISMA coin is not good. The ratio of Ne+U exp is > 1.5."<<std::endl;
	std::cout<<"================================================"<<std::endl;
}

void gammaSpecWithQgate(Int_t aa=22, Int_t zz = 10, Float_t qq = 40){

	gCan->cd(6);
	TH2F *fhGammaQ = (TH2F*) gIptFile->Get(Form("AgataPrisma/Ion_Cuts_Z%d_A%d/m_DC_Qval_ion_%d_%d",zz,aa,aa,zz));
	fhGammaQ->SetName("E_gamma VS Qval");
	fhGammaQ->Draw("colz");
	fhGammaQ->GetXaxis()->SetRangeUser(-20,100);
	gCan->cd(7);

	vector<TH1F*> vhist;
	vhist.clear();
	int binq = fhGammaQ->GetXaxis()->FindBin(qq);
	int fqwidth = 5;
	vhist.push_back((TH1F*)fhGammaQ->ProjectionY(Form("gate on %f",qq),binq-fqwidth,binq+fqwidth));
	vhist.back()->Draw();
	std::cout<<"================================================"<<std::endl;
	std::cout<<"*Current gate width of Q value is +/- "<<fqwidth<<std::endl;
	std::cout<< "*Cut at Qvalue = " << qq << ", corresponding to bin "<<binq<<std::endl; 
	std::cout<<"================================================"<<std::endl;

}

void ana(){
	help();
}

void showAll(){

	prismaCountingRateCalc();
	xfpFocalCalc();
	beamCoulexCalc();
	oxFromIC();
	agataPRISMA_coinCheck();
	gammaSpecWithQgate();

}
